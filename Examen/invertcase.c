#include<stdio.h>
#include<stdlib.h>


void invertcase(char *s){
	while (*s){
		if (*s>= 'a' && *s <= 'z') {
		    *s -= 32; 
		}
		else {
            		*s += 32;
       		}
		*s++;
	}
}

	
int main(int argc, char *argv[]){
	if (argc != 2) {
		printf("Impossible car chaîne de caractère vide %s\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	invertcase(argv[1]);
    	printf("%s\n", argv[1]);
	return EXIT_SUCCESS;
}
