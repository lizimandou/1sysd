#include<stdio.h>
#include<stdlib.h>

long factorial (int n)
{
   if (n < 0) {
      exit(EXIT_FAILURE);
      
   }
   else if (n == 1 || n == 0) {
      return 1;
      //factoriel de 0 est 1 car sinon cela serait impossible de calculer les autres factoriels
   }
 
   return n * factorial (n - 1);
}

int main(int argc, char *argv[1]){
	printf("Veuillez choisir votre nombre\n");
	int nombre;
	scanf("%d",&nombre);
	long result=factorial(nombre);
	printf("Voici le résultat de votre factoriel : %ld\n :",result);
	int start = atoi(argv[1]);
        int end = atoi(argv[2]);

	for(int i=start;i<=end;i++){
		printf("%d! = %d\n", i,factorial(i));
	}
}
